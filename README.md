# Bao

Bao is an email client and personal information manager built with Torty. It can send and receive mail, and it can also serve as a personal calendar and store contact information. Contact lists and calendars can be sequestered from each other or merged into one master contact list or calendar.
